import React, { Component } from 'react';
import HomeLayout from '../component/home-layout';
import BoardLayout from '../../board/component/board-layout';
import WordFieldLayout from '../../word-field/component/word-field-layout';
import ResetLayout from '../../reset/component/reset-layout';
import {board, words} from '../../helpers/json-helper'

class Home extends Component {
  state = {
    word: "",
    lettersId: {}, // Object to preserve Id of every selected tile to make every selection unique. Example: If you writte "whidth" and you're going delete the desire "h" and not the wrong one
    isValid: false,
    resetCount: 0
  }

  componentDidUpdate(prevProps, prevState) {
      if(this.state.word !== prevState.word) {
        if(words.includes(this.state.word.toLowerCase())){
          this.setState({
           isValid: true
          })
        }else{
          this.setState({
            isValid: false
           })
        }
      }
  }

  /**
   * It adds or remove the selected letter to the word in the state 
   * 
   * @param {string} letter - The value of the letter that is going to be added into the word
   * @param {string} id - The ID of the Tile to know in which position the letter is
   * @param {boolean} willAddLetter - If it receives true will add the letter to the word if it doesn't it will remove it
   */
  modifyLetterOnClick = (letter, id, willAddLetter) => { 

    if(willAddLetter){
      let newWord = this.state.word
      const newLettersId = {...this.state.lettersId, [id]: letter}
      
      newWord = Object.values(newLettersId).join('') // Transform object newLettersId to an Array and it joins all of it elements into a String
      
      this.setState({
        word: newWord,
        lettersId: newLettersId
      })

    }else{
      let newWord = this.state.word
      const newLettersId = {...this.state.lettersId}
      delete newLettersId[`${id}`]
      newWord = Object.values(newLettersId).join('')

      this.setState({
        word: newWord,
        lettersId: newLettersId
      })
    }
  }

  /** 
   * Reset the whole game when Reset button is pressed
  */
  handleResetClick = () => { 
    this.setState(prevState => ({
      word: "",
      lettersId: {},
      isValid: false,
      resetCount: prevState.resetCount + 1 // restart the board by changing the key of BoardLayout and mounting a new Board
    }))
    console.log(this.state.resetCount);
  }


  render() {
    return (
      <HomeLayout>
        <ResetLayout
          handleClick={this.handleResetClick}
          isValid={this.state.isValid}
          />
        <BoardLayout 
          modifyLetterOnClick={this.modifyLetterOnClick}
          board={board}
          isValid={this.state.isValid}
          key={this.state.resetCount}
          />
        <WordFieldLayout
          word={this.state.word}
          isValid={this.state.isValid}
        />
      </HomeLayout>
    )
  }
}

export default Home
