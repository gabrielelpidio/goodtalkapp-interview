import { words as w} from '../../files/dictionary.json'
import { board as b} from '../../files/test-board.json' 

export const words = w
/** 
 * Divides the array in test-board.json into arrays of 4. 
 * So the first level of arrays represents each row. 
 * And each value inside the second level of array represents each Tile
*/
export const board = chunkArray(b, 4) 


/**
 * It divides the array into Chunks
 * 
 * @param {Array} arr 
 * @param {number} chunkSize 
 * @returns {Array} Array with arrays of size=chunkSize
 */
function chunkArray(arr, chunkSize){
  const results = [];

  while (arr.length) {
    results.push(arr.splice(0, chunkSize));
  }
  return results;
}


