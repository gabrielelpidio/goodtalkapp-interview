import React, { Component } from 'react'
import { render } from 'react-dom'
import Home from '../home/container/home';

const HomeContainer = document.getElementById('home-container')

render(<Home/>, HomeContainer)

