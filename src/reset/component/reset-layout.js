import React from 'react'
import './reset-layout.css'

const ResetLayout = props => {
  return (
    <div className="Reset-container" onClick={props.handleClick}>
      <div className="Reset-label">
        clear word
      </div>
      <div className="Reset-button">
        X     
      </div>
    </div>
  )
}
export default ResetLayout
