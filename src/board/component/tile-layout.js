import React, { PureComponent } from 'react'
import './tile-layout.css'

const TileLayout = props => {
  const styles = {
    TileContainer: {
      valid: {
        backgroundImage: 'linear-gradient(#b3ec50, #429421)',
        borderColor: 'transparent'
      },
      invalid: {
        backgroundImage: 'linear-gradient(#f34f5e, #9f041b)',
        borderColor: 'transparent'
      }
    }
  }

  const { isActive, isValid } = props
  return(
    <div 
      className={"Tile-container"}
      onClick={props.handleClick}
      style={(isActive && isValid) ? 
                styles.TileContainer.valid 
              : 
              (isActive && !isValid) ? 
                styles.TileContainer.invalid
              :
              null
      }>
      <div className="Tile">
        {props.letter}
      </div>
    </div>
  )
}


export default TileLayout