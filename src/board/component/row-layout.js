import React from 'react'
import './row-layout.css'
import Tile from '../container/tile';

/**
 * It generates Tiles Dynamically going through an array of Strings given in the props 
 * And passes a Letter to each one of its childs
 * 
 * @param {Object} props 
 */
const RowLayout = props => {
  return (
    <div className="row">
      {
        props.letters.map((value, index)=>{
          return <Tile
              modifyLetterOnClick={props.modifyLetterOnClick}
              letter={value}
              isValid={props.isValid}
              key={index}
              id={`${props.id}-${index}`}
            />
        })
      }
    </div>
  )
}

export default RowLayout
