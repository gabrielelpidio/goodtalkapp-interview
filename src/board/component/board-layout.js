import React from 'react'
import './board-layout.css'
import RowLayout from '../component/row-layout';


/**
 * It generates Rows Dynamically going through the first level of an array of arrays given in the props 
 * And passes one of the second level arrays to each child 
 * @param {Object} props 
 */
const BoardLayout = props => {
  return (
    <div className="board">
      {
        props.board.map((value, index) => {
            return <RowLayout
              modifyLetterOnClick={props.modifyLetterOnClick}
              letters={value}
              isValid={props.isValid}
              key={index}
              id={index}
            />
        })
      }
    </div>
  )
}

export default BoardLayout
