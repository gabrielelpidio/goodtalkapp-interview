import React, { Component } from 'react'
import TileLayout from '../component/tile-layout';

class Tile extends Component {
  state = {
    isActive: false,
  }
  
  /**
   * Toggles the tile to active or inactive changing the state and calls a function on parent to modify the word
   */
  
  handleClick = e => {
    if(this.state.isActive === false){
      this.setState({
        isActive: true
      })
      this.props.modifyLetterOnClick(this.props.letter, this.props.id, true) 
    }

    else{
      this.setState({
        isActive: false
      })
      this.props.modifyLetterOnClick(this.props.letter, this.props.id, false)
    }
  }

  render() {
    return (
      <TileLayout
        letter={this.props.letter}
        isActive={this.state.isActive}
        isValid={this.props.isValid}
        handleClick={this.handleClick}
      />
    )
  }
}

export default Tile
