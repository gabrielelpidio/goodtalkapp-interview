import React from 'react'
import './word-field-layout.css'

const WordFieldLayout = props => {

  const styles = {
    WordField: {
      valid: {
        color: '#7ed321'
      },
      invalid: {
        color: '#d0041d'
      }
    },
    WordFieldCheck: {
      valid: {
        color: '#cddabc'
      },
      invalid: {
        color: '#f3bcc2'
      }
      }
    }

    const { isValid } = props
  
    return (
      <div className="Word-field-container">
        <div className="Word-field"  
             style={ isValid ? styles.WordField.valid : styles.WordField.invalid}
        >
          {props.word}
        </div>
        <div className={"Word-field-check"} 
             style={isValid ? styles.WordFieldCheck.valid : styles.WordFieldCheck.invalid}
        >
          { isValid ? "valid" : "invalid" }
        </div>
      </div>
    )
}



export default WordFieldLayout
