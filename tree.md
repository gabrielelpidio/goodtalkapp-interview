```
GoodTalkAppInterview
|
├── dist   >    Production Files folder
│   ├── css
│   │   └── home.css
│   └── js
│       └── home.js
├── files   >   Input Files
│   ├── dictionary.json
│   └── test-board.json
├── src
│   ├── board
│   │   ├── component
│   │   │   ├── board-layout.css
│   │   │   ├── board-layout.js     >   Dynamically  generates Rows of Tiles
│   │   │   ├── row-layout.css
│   │   │   ├── row-layout.js       >   Dynamically  generates Tiles
│   │   │   ├── tile-layout.css     
│   │   │   └── tile-layout.js      >   Tile UI
│   │   └── container
│   │       └── tile.js  >  Controls each tile state 
│   ├── entries
│   │   └── home.js  >  Application Entry Point
│   ├── helpers
│   │   └── json-helper.js  >  Converts JSON Input into valid Arrays input
│   ├── home
│   │   ├── component
│   │   │   ├── home-layout.css
│   │   │   └── home-layout.js      >   It places every component rendered in Home accordingly
│   │   └── container
│   │       └── home.js     >   Renders all components and manages most actions
│   ├── reset
│   │   └── component
│   │       ├── reset-layout.css
│   │       └── reset-layout.js     >   Reset Button
│   └── word-field
│       └── component
│           ├── word-field-layout.css
│           └── word-field-layout.js    > Where word is displayed
├── index.html
├── package.json
├── package-lock.json
├── readme.md
├── to-do.md
├── tree.md
├── webpack.config.js
└── webpack.dev.config.js

```