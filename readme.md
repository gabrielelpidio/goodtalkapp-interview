### This is my solution to the Words Game Challenge

# How to run

## Running in Production environment

  1. Clone or download this repo and go to the folder where its located.

  2. Open `index.html`.

  3. Done.

## Running in Development environment

  1. Install Node.js into your computer.

  2. Clone or download this repo.

  3. Go to the repo folder and open `index.html` in your favourite Text Editor and comment line 7 and 11 and uncomment line 12. So each line would now be:

      * Line 7
        ```
        <!-- <link rel="stylesheet" href="dist/css/home.css"> -->
        ```

      * Line 11
          ```
          <!-- <script src="dist/js/home.js"></script> -->
          ```
      * Line 12
          ```
          <script src="http://localhost:9000/js/home.js"></script>
          ```

  4. Open Terminal or CMD and go to this repo folder with: 

    cd /path/to/repo/

  5. Once you are in the folder install all npm dependencies writing this:
  
    npm install

  6. Now you can run the development server with:
    
    npm run build:dev

  5. Go to your favourite browser and go to `localhost:9000`.

  6. Done.


#### Further information about each component can be found on [treee.md](./tree.md) and on each component of the app.

